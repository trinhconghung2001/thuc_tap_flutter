import 'package:flutter/material.dart';
import 'package:app/Container/Container1.dart';
import 'package:app/Container/ListAppointment.dart';

class ManageCaculator extends StatefulWidget {
  static const routerName = '/manageCaculator';
  const ManageCaculator({super.key});

  @override
  State<ManageCaculator> createState() => _ManageCaculatorState();
}

class _ManageCaculatorState extends State<ManageCaculator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Quản lý lịch hẹn khám CSYT'),
        backgroundColor: Color(0xFF6F9BD4),
      ),
      body: Container(
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 5),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Color.fromARGB(255, 178, 200, 219),
                    ),
                    width: MediaQuery.of(context).size.width,
                    height: 230,
                    child: Contain1(),
                  ),
                  ListAppointment(),
                ],
              ),
            )),
      ),
    );
  }
}
