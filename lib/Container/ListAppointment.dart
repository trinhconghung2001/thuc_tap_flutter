import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/model/ItemAppointment.dart';

class ListAppointment extends StatefulWidget {
  const ListAppointment({super.key});

  @override
  State<ListAppointment> createState() => _ListAppointmentState();
}

class _ListAppointmentState extends State<ListAppointment> {
  List<List<String>> listAppoint = [
    [
      'Mới đăng ký',
      'Chưa đến lịch hẹn',
      'Nguyễn Thị Ánh Nguyệt (Viện phí)',
      'Ngoại tổng hợp',
      'Khám sản',
      '07/07/2021 9:00'
    ],
    [
      'Đã xác nhận',
      'Đến khám muộn',
      'Nguyễn Thị Ánh Nguyệt (BHYT)',
      'Ngoại tổng hợp',
      'Khám sản',
      '07/07/2021 9:00'
    ],
    [
      'Hủy',
      'Đến khám muộn',
      'Nguyễn Thị Ánh Nguyệt (Viện phí)',
      'Ngoại tổng hợp',
      'Khám sản',
      '07/07/2021 9:00',
    ]
  ];
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    // final height_screen = MediaQuery.of(context).size.height;
    return Wrap(children: [
      Container(
          width: width_screen,
          child: ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: listAppoint.length,
            itemBuilder: (BuildContext context, int index) {
              return ItemAppoinment(
                  title: listAppoint[index][0],
                  valueSate: listAppoint[index][1],
                  valueActor: listAppoint[index][2],
                  valueLocation: listAppoint[index][3],
                  valueCause: listAppoint[index][4],
                  valueDate: listAppoint[index][5],
                  onPressed: () {});
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
          )),
    ]);
  }
}
