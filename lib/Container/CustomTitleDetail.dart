import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomTitleDetail extends StatefulWidget {
  final String keyItem;
  final String valueItem;
  // final SvgPicture iconItem;
  const CustomTitleDetail(
      {super.key,
      required this.keyItem,
      required this.valueItem,
      });
  @override
  State<CustomTitleDetail> createState() => _CustomTitleDetailState();
}

class _CustomTitleDetailState extends State<CustomTitleDetail> {
  @override
  Widget build(BuildContext context) {
    Color colorValue;
    Color colorTitle;
    if (widget.valueItem == 'Đến khám muộn') {
      // textColor = Color(0xFF5CBBB8);
      colorValue = Color(0xFFB43939);
    } else if (widget.valueItem == 'Đã đến khám') {
      colorValue = Color(0xFF5CBBB8);
    } else {
      colorValue = Color(0xFF535858);
    }

    if (widget == 'Số thẻ BHYT (*)') {
      colorTitle = Color(0xFFB43939);
      colorValue = Color(0xFFB43939);
    } else {
      colorTitle = Color(0xFF535858);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 2,
            child: Container(
                height: 20,
                child: Text(widget.keyItem,
                    style: TextStyle(fontSize: 11, color: colorTitle)))),
        Expanded(
          flex: 4,
            child: Container(
          height: 20,
          child: Text(widget.valueItem,
              style: TextStyle(fontSize: 11, color: colorValue)),
        )),
        // Expanded(child: widget.iconItem,flex: 1,),
      ],
    );
  }
}
