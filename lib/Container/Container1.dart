import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app/Container/CustomButton.dart';
import 'package:app/Container/CustomContainer.dart';
import 'package:app/Container/DetailSpinner.dart';

class Contain1 extends StatefulWidget {
  const Contain1({
    super.key,
  });

  @override
  State<Contain1> createState() => _Contain1State();
}

class _Contain1State extends State<Contain1> {
  late String idSelected;
  Map<String, String> kqua = {};
  String resultPay = 'Tất cả',
      resultRegistry = 'Tất cả',
      resultState = 'Tất cả',
      resultStatus = 'Tất cả';
  void setStatus(String id) {
    setState(() {
      idSelected = id;
    });
  }

  DateTimeRange dateTimeRange =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    void tranfer() async {
      final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailSpinner(value1: idSelected)),
      );
      if (result != null) {
        setState(() {
          kqua = result;
          if(kqua['idPay'] != null){
             resultPay = kqua['idPay']!;
          }
          if(kqua['idRegistry'] != null){
            resultRegistry = kqua['idRegistry']!;
          }
         if(kqua['idState'] != null){
            resultState = kqua['idState']!;
          }
          if(kqua['idStatus'] != null){
            resultStatus = kqua['idStatus']!;
          } 
        });
      }
    }

    // final height_screen = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          width: double.infinity,
          height: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomContainer(
                containerText: 'Từ ngày - Đến ngày',
                buttonText: '${formatDate(dateTimeRange.start, [
                      dd,
                      '/',
                      mm,
                      '/',
                      yyyy
                    ])} - ${formatDate(dateTimeRange.end, [
                      dd,
                      '/',
                      mm,
                      '/',
                      yyyy
                    ])}',
                buttonIcon: Icons.calendar_month,
                onPressed: () {
                  _show();
                },
                width: width_screen,
                height: 30,
              ),
              SizedBox(height: 5),
              Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomContainer(
                    containerText: 'Đóng phí',
                    // buttonText: '${kqua['idPay']}',
                    buttonText: resultPay,
                    buttonIcon: Icons.arrow_drop_down_sharp,
                    onPressed: () {
                      setStatus('idPay');
                      tranfer();
                    },
                    width: 148,
                    height: 30,
                  ),
                  CustomContainer(
                    containerText: 'Kênh đăng ký',
                    // buttonText: '${kqua['idRegistry']}',
                    buttonText: resultRegistry,
                    buttonIcon: Icons.arrow_drop_down_sharp,
                    onPressed: () {
                      setStatus('idRegistry');
                      tranfer();
                    },
                    width: 148,
                    height: 30,
                  ),
                ],
              )),
              SizedBox(height: 5),
              Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CustomContainer(
                    containerText: 'Trạng thái',
                    // buttonText: '${kqua['idState']}',
                    buttonText: resultState,
                    buttonIcon: Icons.arrow_drop_down_sharp,
                    onPressed: () {
                      setStatus('idState');
                      tranfer();
                    },
                    width: 148,
                    height: 30,
                  ),
                  CustomContainer(
                    containerText: 'Trạng thái đến khám',
                    // buttonText: '${kqua['idStatus']}',
                    buttonText: resultStatus,
                    buttonIcon: Icons.arrow_drop_down_sharp,
                    onPressed: () {
                      setStatus('idStatus');
                      tranfer();
                    },
                    width: 148,
                    height: 30,
                  ),
                ],
              )),
              SizedBox(height: 10),
              Container(
                width: width_screen,
                height: 30,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CustomButton(
                          width: 148,
                          height: 30,
                          textButton: 'Tìm kiếm',
                          buttonIcon: SvgPicture.asset('assets/search.svg'),
                          onPressed: () {}),
                      CustomButton(
                          width: 148,
                          height: 30,
                          textButton: 'Thêm mới',
                          buttonIcon: SvgPicture.asset('assets/add.svg'),
                          onPressed: () {})
                    ]),
              )
            ],
          )),
    );
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime.now(),
      lastDate: DateTime(2030, 12, 31),
      currentDate: DateTime.now(),
      saveText: 'OK',
      initialDateRange: dateTimeRange,
      
    );
    if (result != null) {
      // Rebuild the UI
      print(result.start.toString());
      print(result.end.toString());
      setState(() {
        dateTimeRange = result;
      });
    }
  }
}
